; RUN: opt -S -load %dylibdir/libLICM.so \
; RUN:     -loop-invariant-code-motion %s -o %basename_t 2>%basename_t.log
; RUN: FileCheck --match-full-lines --check-prefix=CODEGEN %s --input-file=%basename_t
; RUN: llc -load %dylibdir/libLICM.so -regalloc=intfgraph %basename_t -o %basename_t.s
; RUN: clang %basename_t.s -o %basename_t.exe
; RUN: ./%basename_t.exe | FileCheck --match-full-lines --check-prefix=CORRECTNESS %s
; CORRECTNESS: 3,4,10,6,7,12,3,10
; CORRECTNESS-NEXT: 8,4,0,0,7,0,3,13

; #include <stdio.h>

; void foo(int c, int z) {
;   int a = 9, h, m = 0, n = 0, q, r = 0, y = 0;

; LOOP:
;   z = z + 1;
;   y = c + 3;
;   q = c + 7;
;   if (z < 5) {
;     a = a + 2;
;     h = c + 3;
;   } else {
;     a = a - 1;
;     h = c + 4;
;     if (z >= 10) {
;       goto EXIT;
;     }
;   }
;   m = y + 7;
;   n = h + 2;
;   y = c + 7;
;   r = q + 5;
;   goto LOOP;
; EXIT:
;   printf("%d,%d,%d,%d,%d,%d,%d,%d\n", a, h, m, n, q, r, y, z);
; }

; int main() {
;   foo(0, 4);
;   foo(0, 12);
;   return 0;
; }
@.str = private constant [25 x i8] c"%d,%d,%d,%d,%d,%d,%d,%d\0A\00", align 1

define void @foo(i32 %0, i32 %1) {
; CODEGEN-LABEL:   %3 = add nsw i32 %0, 3
; CODEGEN-NEXT:   %4 = add nsw i32 %0, 7
; CODEGEN-NEXT:   %5 = add nsw i32 %0, 3
; CODEGEN-NEXT:   %6 = add nsw i32 %3, 7
; CODEGEN-NEXT:   %7 = add nsw i32 %0, 7
; CODEGEN-NEXT:   %8 = add nsw i32 %4, 5
; CODEGEN-NEXT:   %9 = add nsw i32 %0, 4
; CODEGEN-NEXT:   br label %10
; CODEGEN-LABEL: 10:                                               ; preds = %18, %2
; CODEGEN-NEXT:   %.05 = phi i32 [ 0, %2 ], [ %8, %18 ]
; CODEGEN-NEXT:   %.04 = phi i32 [ 0, %2 ], [ %19, %18 ]
; CODEGEN-NEXT:   %.03 = phi i32 [ 0, %2 ], [ %6, %18 ]
; CODEGEN-NEXT:   %.01 = phi i32 [ 9, %2 ], [ %.1, %18 ]
; CODEGEN-NEXT:   %.0 = phi i32 [ %1, %2 ], [ %11, %18 ]
; CODEGEN-NEXT:   %11 = add nsw i32 %.0, 1
; CODEGEN-NEXT:   %12 = icmp slt i32 %11, 5
; CODEGEN-NEXT:   br i1 %12, label %13, label %15
; CODEGEN-LABEL: 13:                                               ; preds = %10
; CODEGEN-NEXT:   %14 = add nsw i32 %.01, 2
; CODEGEN-NEXT:   br label %18
; CODEGEN-LABEL: 15:                                               ; preds = %10
; CODEGEN-NEXT:   %16 = sub nsw i32 %.01, 1
; CODEGEN-NEXT:   %17 = icmp sge i32 %11, 10
; CODEGEN-NEXT:   br i1 %17, label %20, label %18
; CODEGEN-LABEL: 18:                                               ; preds = %15, %13
; CODEGEN-NEXT:   %.02 = phi i32 [ %5, %13 ], [ %9, %15 ]
; CODEGEN-NEXT:   %.1 = phi i32 [ %14, %13 ], [ %16, %15 ]
; CODEGEN-NEXT:   %19 = add nsw i32 %.02, 2
; CODEGEN-NEXT:   br label %10
; CODEGEN-LABEL: 20:                                               ; preds = %15
; CODEGEN-NEXT:   %21 = add nsw i32 %0, 7
; CODEGEN-NEXT:   %22 = add nsw i32 %0, 3
; CODEGEN-NEXT:   %23 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str, i64 0, i64 0), i32 %16, i32 %9, i32 %.03, i32 %.04, i32 %21, i32 %.05, i32 %22, i32 %11)
; CODEGEN-NEXT:   ret void

  br label %3

3:                                                ; preds = %15, %2
  %.05 = phi i32 [ 0, %2 ], [ %19, %15 ]
  %.04 = phi i32 [ 0, %2 ], [ %17, %15 ]
  %.03 = phi i32 [ 0, %2 ], [ %16, %15 ]
  %.01 = phi i32 [ 9, %2 ], [ %.1, %15 ]
  %.0 = phi i32 [ %1, %2 ], [ %4, %15 ]
  %4 = add nsw i32 %.0, 1
  %5 = add nsw i32 %0, 3
  %6 = add nsw i32 %0, 7
  %7 = icmp slt i32 %4, 5
  br i1 %7, label %8, label %11

8:                                                ; preds = %3
  %9 = add nsw i32 %.01, 2
  %10 = add nsw i32 %0, 3
  br label %15

11:                                               ; preds = %3
  %12 = sub nsw i32 %.01, 1
  %13 = add nsw i32 %0, 4
  %14 = icmp sge i32 %4, 10
  br i1 %14, label %20, label %15

15:                                               ; preds = %11, %8
  %.02 = phi i32 [ %10, %8 ], [ %13, %11 ]
  %.1 = phi i32 [ %9, %8 ], [ %12, %11 ]
  %16 = add nsw i32 %5, 7
  %17 = add nsw i32 %.02, 2
  %18 = add nsw i32 %0, 7
  %19 = add nsw i32 %6, 5
  br label %3

20:                                               ; preds = %11
  %21 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str, i64 0, i64 0), i32 %12, i32 %13, i32 %.03, i32 %.04, i32 %6, i32 %.05, i32 %5, i32 %4)
  ret void
}

declare i32 @printf(i8*, ...)

define i32 @main() {
  call void @foo(i32 0, i32 4)
  call void @foo(i32 0, i32 12)
  ret i32 0
}


; official pass result:
; define void @foo(i32 %0, i32 %1) {
;  %3 = add nsw i32 %0, 4
;  %4 = add nsw i32 %0, 3
;  %5 = add nsw i32 %0, 10
;  %6 = add nsw i32 %0, 12
;  br label %7
;7:                                                ; preds = %15, %2
;  %.05 = phi i32 [ 0, %2 ], [ %6, %15 ]
;  %.04 = phi i32 [ 0, %2 ], [ %16, %15 ]
;  %.03 = phi i32 [ 0, %2 ], [ %5, %15 ]
;  %.01 = phi i32 [ 9, %2 ], [ %.1, %15 ]
;  %.0 = phi i32 [ %1, %2 ], [ %8, %15 ]
;  %8 = add nsw i32 %.0, 1
;  %9 = icmp slt i32 %.0, 4
;  br i1 %9, label %10, label %12
;10:                                               ; preds = %7
;  %11 = add nsw i32 %.01, 2
;  br label %15
;12:                                               ; preds = %7
;  %13 = add nsw i32 %.01, -1
;  %14 = icmp sgt i32 %.0, 8
;  br i1 %14, label %17, label %15
;15:                                               ; preds = %12, %10
;  %.02 = phi i32 [ %4, %10 ], [ %3, %12 ]
;  %.1 = phi i32 [ %11, %10 ], [ %13, %12 ]
;  %16 = add nsw i32 %.02, 2
;  br label %7
;17:                                               ; preds = %12
;  %18 = add nsw i32 %0, 7
;  %19 = add nsw i32 %0, 3
;  %20 = %20 = call i32 (i8*, ...) @printf(i8* nonnull dereferenceable(1) getelementptr inbounds ([25 x i8], [25 x i8]* @.str, i64 0, i64 0), i32 %13, i32 %3, i32 %.03, i32 %.04, i32 %18, i32 %.05, i32 %19, i32 %8)
;  ret void
;}
; declare i32 @printf(i8*, ...)
; define i32 @main() {
;   call void @foo(i32 0, i32 4)
;   call void @foo(i32 0, i32 12)
;   ret i32 0
; }