/**
 * @file Interference Graph Register Allocator
 */
#include <llvm/Analysis/AliasAnalysis.h>
#include <llvm/CodeGen/LiveIntervals.h>
#include <llvm/CodeGen/LiveRangeEdit.h>
#include <llvm/CodeGen/LiveRegMatrix.h>
#include <llvm/CodeGen/LiveStacks.h>
#include <llvm/CodeGen/MachineBlockFrequencyInfo.h>
#include <llvm/CodeGen/MachineDominators.h>
#include <llvm/CodeGen/MachineFunctionPass.h>
#include <llvm/CodeGen/MachineLoopInfo.h>
#include <llvm/CodeGen/RegAllocRegistry.h>
#include <llvm/CodeGen/RegisterClassInfo.h>
#include <llvm/CodeGen/Spiller.h>
#include <llvm/CodeGen/TargetRegisterInfo.h>
#include <llvm/CodeGen/VirtRegMap.h>
#include <llvm/InitializePasses.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Target/TargetMachine.h>

#include <cmath>
#include <queue>
#include <tuple>
#include <unordered_map>
#include <unordered_set>

using namespace llvm;

namespace llvm {

void initializeRAIntfGraphPass(PassRegistry &Registry);

} // namespace llvm

namespace std {

template <> //
struct hash<Register> {
  size_t operator()(const Register &Reg) const {
    return DenseMapInfo<Register>::getHashValue(Reg);
  }
};

template <> //
struct greater<LiveInterval *> {
  bool operator()(LiveInterval *const &LHS, LiveInterval *const &RHS) {
    return LHS->weight() > RHS->weight();
  }
};

} // namespace std

namespace {

class RAIntfGraph;

class AllocationHints {
private:
  SmallVector<MCPhysReg, 16> Hints;

public:
  AllocationHints(RAIntfGraph *const RA, const LiveInterval *const LI);
  SmallVectorImpl<MCPhysReg>::iterator begin() { return Hints.begin(); }
  SmallVectorImpl<MCPhysReg>::iterator end() { return Hints.end(); }
};

class RAIntfGraph final : public MachineFunctionPass,
                          private LiveRangeEdit::Delegate {
private:
  MachineFunction *MF;
  SlotIndexes *SI;
  VirtRegMap *VRM;
  const TargetRegisterInfo *TRI;
  MachineRegisterInfo *MRI;
  RegisterClassInfo RCI;
  LiveRegMatrix *LRM;
  MachineLoopInfo *MLI;
  LiveIntervals *LIS;

  /**
   * @brief Interference Graph
   */
  class IntfGraph {
  private:
    RAIntfGraph *RA;

    /// Interference Relations
    std::multimap<LiveInterval *, std::unordered_set<Register>,
                  std::greater<LiveInterval *>>
        IntfRels;

    /**
     * @brief  Try to materialize all the virtual registers (internal).
     *
     * @return (nullptr, VirtPhysRegMap) in the case when a successful
     *         materialization is made, (LI, *) in the case when unsuccessful
     *         (and LI is the live interval to spill)
     *
     * @sa tryMaterializeAll
     */
    using MaterializeResult_t =
        std::tuple<LiveInterval *,
                   std::unordered_map<LiveInterval *, MCPhysReg>>;
    MaterializeResult_t tryMaterializeAllInternal();
    void updateWeight(const Register &Reg,
                      std::unordered_set<Register> &RegSet);

  public:
    explicit IntfGraph(RAIntfGraph *const RA) : RA(RA) {}
    /**
     * @brief Insert a virtual register @c Reg into the interference graph.
     */
    void insert(const Register &Reg);
    /**
     * @brief Erase a virtual register @c Reg from the interference graph.
     *
     * @sa RAIntfGraph::LRE_CanEraseVirtReg
     */
    void erase(const Register &Reg);
    /**
     * @brief Build the whole graph.
     */
    void build();
    /**
     * @brief Try to materialize all the virtual registers.
     */
    void tryMaterializeAll();
    void clear() { IntfRels.clear(); }
  } G;

  SmallPtrSet<MachineInstr *, 32> DeadRemats;
  std::unique_ptr<Spiller> SpillerInst;

  void postOptimization() {
    SpillerInst->postOptimization();
    for (MachineInstr *const DeadInst : DeadRemats) {
      LIS->RemoveMachineInstrFromMaps(*DeadInst);
      DeadInst->eraseFromParent();
    }
    DeadRemats.clear();
    G.clear();
  }

  friend class AllocationHints;
  friend class IntfGraph;

  /// The following two methods are inherited from @c LiveRangeEdit::Delegate
  /// and implicitly used by the spiller to edit the live ranges.
  bool LRE_CanEraseVirtReg(Register Reg) override {
    // If the virtual register has been materialized, undo its physical
    // assignment and erase it from the interference graph.
    G.erase(Reg);
    return true;
  }

  void LRE_WillShrinkVirtReg(Register Reg) override {
    // If the virtual register has been materialized, undo its physical
    // assignment and re-insert it into the interference graph.
    G.insert(Reg);
  }

public:
  static char ID;

  StringRef getPassName() const override {
    return "Interference Graph Register Allocator";
  }

  RAIntfGraph() : MachineFunctionPass(ID), G(this) {}

  void getAnalysisUsage(AnalysisUsage &AU) const override {
    MachineFunctionPass::getAnalysisUsage(AU);
    AU.setPreservesCFG();
#define REQUIRE_AND_PRESERVE_PASS(PassName)                                    \
  AU.addRequired<PassName>();                                                  \
  AU.addPreserved<PassName>()

    REQUIRE_AND_PRESERVE_PASS(SlotIndexes);
    REQUIRE_AND_PRESERVE_PASS(VirtRegMap);
    REQUIRE_AND_PRESERVE_PASS(LiveIntervals);
    REQUIRE_AND_PRESERVE_PASS(LiveRegMatrix);
    REQUIRE_AND_PRESERVE_PASS(LiveStacks);
    REQUIRE_AND_PRESERVE_PASS(AAResultsWrapperPass);
    REQUIRE_AND_PRESERVE_PASS(MachineDominatorTree);
    REQUIRE_AND_PRESERVE_PASS(MachineLoopInfo);
    REQUIRE_AND_PRESERVE_PASS(MachineBlockFrequencyInfo);
  }

  MachineFunctionProperties getRequiredProperties() const override {
    return MachineFunctionProperties().set(
        MachineFunctionProperties::Property::NoPHIs);
  }
  MachineFunctionProperties getClearedProperties() const override {
    return MachineFunctionProperties().set(
        MachineFunctionProperties::Property::IsSSA);
  }

  bool runOnMachineFunction(MachineFunction &MF) override;
}; // class RAIntfGraph

AllocationHints::AllocationHints(RAIntfGraph *const RA,
                                 const LiveInterval *const LI) {
  const TargetRegisterClass *const RC = RA->MRI->getRegClass(LI->reg());
  ArrayRef<MCPhysReg> Order =
      RA->RCI.getOrder(RA->MF->getRegInfo().getRegClass(LI->reg()));
  bool IsHardHint = RA->TRI->getRegAllocationHints(LI->reg(), Order, Hints,
                                                   *RA->MF, RA->VRM, RA->LRM);
  if (!IsHardHint) {
    for (const MCPhysReg &PhysReg : Order) {
      Hints.push_back(PhysReg);
    }
  }
  outs() << "Hint Registers for Class " << RA->TRI->getRegClassName(RC)
         << ": [";
  for (const MCPhysReg &PhysReg : Hints) {
    outs() << RA->TRI->getRegAsmName(PhysReg) << ", ";
  }
  outs() << "]\n";
}

bool RAIntfGraph::runOnMachineFunction(MachineFunction &MF) {
  outs() << "************************************************\n"
         << "* Machine Function\n"
         << "************************************************\n";
  SI = &getAnalysis<SlotIndexes>();
  for (const MachineBasicBlock &MBB : MF) {
    MBB.print(outs(), SI);
    outs() << "\n";
  }
  outs() << "\n\n";

  this->MF = &MF;

  VRM = &getAnalysis<VirtRegMap>();
  TRI = &VRM->getTargetRegInfo();
  MRI = &VRM->getRegInfo();
  MRI->freezeReservedRegs(MF);
  LIS = &getAnalysis<LiveIntervals>();
  LRM = &getAnalysis<LiveRegMatrix>();
  RCI.runOnMachineFunction(MF);
  MLI = &getAnalysis<MachineLoopInfo>();

  SpillerInst.reset(createInlineSpiller(*this, MF, *VRM));

  G.build();
  G.tryMaterializeAll();

  postOptimization();
  return true;
}

void RAIntfGraph::IntfGraph::updateWeight(
    const Register &Reg, std::unordered_set<Register> &RegSet) {
  LiveInterval *RegInterval = &RA->LIS->getInterval(Reg);
  float Accumulator = 0;
  std::pair<bool, bool> RWVirtReg;
  for (auto RegInstr = RA->MRI->reg_instr_begin(RegInterval->reg());
       RegInstr != RA->MRI->reg_instr_end(); ++RegInstr) {
    unsigned LoopDepth = RA->MLI->getLoopDepth(RegInstr->getParent());
    RWVirtReg = RegInstr->readsWritesVirtualRegister(Reg);
    int NumUsesDef = RWVirtReg.first + RWVirtReg.second;
    Accumulator += (NumUsesDef * std::pow(10, LoopDepth) / (RegSet.size() + 1));
  }
  RegInterval->setWeight(Accumulator);
}

void RAIntfGraph::IntfGraph::insert(const Register &Reg) {
  // 1. Collect all VIRTUAL registers that interfere with 'Reg'.
  std::unordered_set<Register> RegSet;
  LiveInterval *RegInterval = &RA->LIS->getInterval(Reg);
  for (unsigned I = 0, E = RA->MRI->getNumVirtRegs(); I != E; ++I) {
    // reg ID
    Register CurReg = Register::index2VirtReg(I);
    if (RA->MRI->reg_nodbg_empty(CurReg)) {
      continue;
    }
    // Get the virtual reg
    LiveInterval *CurLiveInterval = &RA->LIS->getInterval(CurReg);
    errs() << printReg(Reg, RA->TRI, 0, RA->MRI) << "\n";
    if (Reg == CurLiveInterval->reg()) {
      continue;
    }
    if (CurLiveInterval->overlaps(*RegInterval)) {
      RegSet.emplace(CurReg);
      auto SetIter = IntfRels.find(RegInterval);
      if (SetIter != IntfRels.end()) {
        updateWeight(CurReg, SetIter->second);
      }
    }
  }

  // 2. Collect all PHYSICAL registers that interfere with 'Reg'.
  AllocationHints AHints(RA, RegInterval);
  for (MCRegister PhysReg : AHints) {
    switch (RA->LRM->checkInterference(*RegInterval, PhysReg)) {
    case LiveRegMatrix::IK_Free:
      continue;
    default:
      RegSet.emplace(Register(PhysReg));
      break;
    }
  }
  // 3. Update the weights of Reg and all its interfering neighbors, using the
  //    formula on "Lecture 6 Register Allocation Page 23".
  updateWeight(Reg, RegSet);
  // 4. Insert 'Reg' into the graph.
  IntfRels.emplace(RegInterval, RegSet);
}

void RAIntfGraph::IntfGraph::erase(const Register &Reg) {
  // 1. ∀n ∈ neighbors(Reg), erase 'Reg' from n's interfering set and update its
  //    weights accordingly.
  // 2. Erase 'Reg' from the interference graph.
  for (auto Iter = IntfRels.begin(); Iter != IntfRels.end(); ++Iter) {
    size_t Removed = Iter->second.erase(Reg);
    if (Removed > 0) {
      LiveInterval *CurVirtReg = Iter->first;
      updateWeight(CurVirtReg->reg(), Iter->second);
    }
  }
  IntfRels.erase(&RA->LIS->getInterval(Reg));
}

void RAIntfGraph::IntfGraph::build() {
  for (unsigned I = 0, E = RA->MRI->getNumVirtRegs(); I != E; ++I) {
    // reg ID
    Register CurReg = Register::index2VirtReg(I);
    if (RA->MRI->reg_nodbg_empty(CurReg)) {
      continue;
    }
    // insert curreg to the graph
    insert(CurReg);
  }
}

RAIntfGraph::IntfGraph::MaterializeResult_t
RAIntfGraph::IntfGraph::tryMaterializeAllInternal() {
  std::unordered_map<LiveInterval *, MCPhysReg> PhysRegAssignment;
  // ∀r ∈ IntfRels.keys, try to materialize it. If successful, cache it in
  // PhysRegAssignment, else mark it as to be spilled.

  // go thorugh each node in order
  for (auto Iter = IntfRels.begin(); Iter != IntfRels.end(); ++Iter) {
    LiveInterval *CurLI = Iter->first;
    std::unordered_set<Register> Edges = Iter->second;
    // try each possible color for this node
    AllocationHints AHints(RA, CurLI);
    bool IsColourable = true;
    for (auto *Hint = AHints.begin(); Hint != AHints.end(); ++Hint) {
      IsColourable = true;
      // for each colour check for conflict with neighbors
      for (auto Neighbor = Edges.begin(); Neighbor != Edges.end(); ++Neighbor) {
        // if it is a virtual register
        if (Neighbor->isVirtual()) {
          // check if it has been assigned a colour
          if (PhysRegAssignment.find(&RA->LIS->getInterval(*Neighbor)) ==
              PhysRegAssignment.end()) {
            // if not continue to check other neighbors
            continue;
          }
          // if colour exist, check if Neighbor has the same colour
          if (RA->TRI->regsOverlap(
                  PhysRegAssignment[&RA->LIS->getInterval(*Neighbor)], *Hint)) {
            // if it actually have the same colour, skip this colour
            IsColourable = false;
            break;
          }
        } else {
          // if this is a physical reg, directly check if the colour is he
          // register itself? by check interference?
          if (RA->TRI->regsOverlap(*Neighbor, *Hint)) {
            IsColourable = false;
            break;
          }
        }
      }
      // after checking all neighbors if this is colourable, assign this colour
      // to this node
      if (IsColourable) {
        PhysRegAssignment.emplace(CurLI, *Hint);
        // then move on the the next node
        break;
      }
    }
    // if all colour has been tested but none of them worked, mark as spilled
    if (!IsColourable) {
      return std::make_tuple(CurLI, PhysRegAssignment);
    }
  }

  return std::make_tuple(nullptr, PhysRegAssignment);
}

void RAIntfGraph::IntfGraph::tryMaterializeAll() {
  std::unordered_map<LiveInterval *, MCPhysReg> PhysRegAssignment;
  // Keep looping until a valid assignment is made. In the case of spilling,
  // modify the interference graph accordingly.
  LiveInterval *LI;
  while (true) {
    MaterializeResult_t MaterializeResult = tryMaterializeAllInternal();
    LI = std::get<0>(MaterializeResult);
    PhysRegAssignment = std::get<1>(MaterializeResult);
    if (LI) {
      // spill LI
      // avoid duplicates
      if (!RA->VRM->hasPhys(LI->reg())) {
        continue;
      }
      // Deallocate the interfering virtual registers.
      SmallVector<Register, 4> SplitVirtRegs;
      RA->LRM->unassign(*LI);
      LiveRangeEdit LRE(LI, SplitVirtRegs, *RA->MF, *RA->LIS, RA->VRM, RA,
                        &RA->DeadRemats);
      RA->SpillerInst->spill(LRE);

    } else {
      // done, assignment is valid
      break;
    }
  }

  for (auto &PhysRegAssignPair : PhysRegAssignment) {
    RA->LRM->assign(*PhysRegAssignPair.first, PhysRegAssignPair.second);
  }
}

char RAIntfGraph::ID = 0;

static RegisterRegAlloc X("intfgraph", "Interference Graph Register Allocator",
                          []() -> FunctionPass * { return new RAIntfGraph(); });

} // anonymous namespace

INITIALIZE_PASS_BEGIN(RAIntfGraph, "regallointfgraph",
                      "Interference Graph Register Allocator", false, false)
INITIALIZE_PASS_DEPENDENCY(SlotIndexes)
INITIALIZE_PASS_DEPENDENCY(VirtRegMap)
INITIALIZE_PASS_DEPENDENCY(LiveIntervals)
INITIALIZE_PASS_DEPENDENCY(LiveRegMatrix)
INITIALIZE_PASS_DEPENDENCY(LiveStacks);
INITIALIZE_PASS_DEPENDENCY(AAResultsWrapperPass);
INITIALIZE_PASS_DEPENDENCY(MachineDominatorTree);
INITIALIZE_PASS_DEPENDENCY(MachineLoopInfo);
INITIALIZE_PASS_DEPENDENCY(MachineBlockFrequencyInfo);
INITIALIZE_PASS_END(RAIntfGraph, "regallointfgraph",
                    "Interference Graph Register Allocator", false, false)
