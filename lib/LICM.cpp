/**
 * @file Loop Invariant Code Motion
 */
#include "llvm/IR/Instruction.h"
#include <llvm/Analysis/LoopPass.h>
#include <llvm/Analysis/ValueTracking.h>
#include <llvm/IR/Dominators.h>
#include "llvm/Transforms/Utils.h"
#include <unordered_map>

using namespace llvm;
namespace {

enum MoveLocation { Hoisting, Sinking, HoistingAndSinking };

class LoopInvariantCodeMotion final : public LoopPass {
public:
  static char ID;

  LoopInvariantCodeMotion() : LoopPass(ID) {}

private:
  DominatorTree *DomTree;
  std::vector<Instruction *> LoopInvariants;
  std::unordered_map<Instruction *, MoveLocation> InvariantMoveMap;

  inline bool invariantsContain(Instruction *I) {
    return std::find(LoopInvariants.begin(), LoopInvariants.end(), I) !=
           LoopInvariants.end();
  }

  inline bool shouldMove(Instruction *I) {
    return InvariantMoveMap.find(I) != InvariantMoveMap.end();
  }

  // we only call shouldSink() for invariants
  bool shouldSink(Instruction &I, Loop *Loop) {
    if (!isDomExitBlocks(&I, Loop)) {
      return false;
    }
    for (auto It = I.user_begin(); It != I.user_end(); ++It) {
      Instruction *User = dyn_cast<Instruction>(*It);
      if (Loop->contains(User->getParent()) && !invariantsContain(User) &&
          !isInvariant(User, Loop)) {
        return false;
      }
    }
    return true;
  }

  // assumed should sink
  bool shouldHoistAndSink(Instruction &I, Loop *Loop) {
    for (auto It = I.user_begin(); It != I.user_end(); ++It) {
      Instruction *User = dyn_cast<Instruction>(*It);
      if (Loop->contains(User->getParent()) && !shouldSink(*User, Loop)) {
        return true;
      }
    }
    return false;
  }

  bool isInvariant(Instruction *I, Loop *Loop) {
    for (auto *Operand : I->operand_values()) {
      if (isa<Constant>(Operand) || isa<Argument>(Operand)) {
        continue;
      }
      if (Instruction *OperandInstr = dyn_cast<Instruction>(Operand)) {
        if (!invariantsContain(OperandInstr) &&
            Loop->contains(OperandInstr->getParent())) {
          return false;
        }
      }
    }
    return isSafeToSpeculativelyExecute(I) && !I->mayReadFromMemory() &&
           !isa<LandingPadInst>(I);
  }

  bool isDomExitBlocks(Instruction *Instr, Loop *Loop) {
    // According to safe suggestions by loop simplify pass, still use
    // getExitBlocks
    SmallVector<BasicBlock *, 0> ExitBlocks;
    Loop->getExitBlocks(ExitBlocks);
    for (auto *BB : ExitBlocks) {
      if (!DomTree->dominates(Instr->getParent(), BB)) {
        return false;
      }
    }
    return true;
  }

  bool invariantUpdate(Loop *L) {
    bool Changed = false;
    for (auto Node = GraphTraits<DominatorTree *>::nodes_begin(DomTree);
         Node != GraphTraits<DominatorTree *>::nodes_end(DomTree); ++Node) {
      BasicBlock *BB = Node->getBlock();
      for (auto &Instr : *BB) {
        if (isInvariant(&Instr, L) && !invariantsContain(&Instr)) {
          LoopInvariants.emplace_back(&Instr);
          Changed = true;
          if (shouldSink(Instr, L)) {
            if (shouldHoistAndSink(Instr, L)) {
              InvariantMoveMap[&Instr] = HoistingAndSinking;
            } else {
              InvariantMoveMap[&Instr] = Sinking;
            }
          } else {
            InvariantMoveMap[&Instr] = Hoisting;
          }
        }
      }
    }
    return Changed;
  }

  void move(Instruction *I, Loop *L) {
    switch (InvariantMoveMap[I]) {
    case Sinking:
      I->moveBefore(L->getExitBlock()->getFirstNonPHIOrDbg());
      break;
    case Hoisting:
      I->moveBefore(L->getLoopPreheader()->getTerminator());
      break;
    case HoistingAndSinking:
      auto *Duplicate = I->clone();
      Duplicate->insertBefore(L->getLoopPreheader()->getTerminator());
      I->replaceUsesWithIf(Duplicate, [&L](Use &U) {
        auto *UserInstr = dyn_cast<Instruction>(U.getUser());
        return !UserInstr || L->contains(UserInstr->getParent());
      });
      I->moveBefore(L->getExitBlock()->getFirstNonPHIOrDbg());
      break;
    }
  }

public:
  virtual void getAnalysisUsage(AnalysisUsage &AU) const override {
    AU.addRequired<DominatorTreeWrapperPass>();
    AU.addRequiredID(LoopSimplifyID);
    AU.setPreservesCFG();
  }

  virtual bool runOnLoop(Loop *L, LPPassManager &LPM) override {
    DomTree = &(getAnalysis<DominatorTreeWrapperPass>().getDomTree());
    if (!L->getLoopPreheader()) { 
      return false;
    }
    while (invariantUpdate(L)) {
    }
    for (auto *Instr : LoopInvariants) {
      if (shouldMove(Instr)) {
        move(Instr, L);
      }
    }
    return true;
  }
};

char LoopInvariantCodeMotion::ID = 0;
RegisterPass<LoopInvariantCodeMotion> X("loop-invariant-code-motion",
                                        "Loop Invariant Code Motion");

} // anonymous namespace
